from pathlib import Path

import cv2
import matplotlib.pyplot as plt
import requests
from io import BytesIO
import numpy as np

class ImageProcessor:
    """
    Return array of character imgs, num of rows & cols in matrix character
    """
    @classmethod
    def convert_to_emnist(self, filepath):
        sample_img = cv2.imread(filepath)
        sample_img = cv2.cvtColor(sample_img, cv2.COLOR_BGR2GRAY)
        resized = cv2.resize(sample_img, (1000, 1000), interpolation = cv2.INTER_AREA)
        filter = np.array([[-1, -1, -1], [-1, 10, -1], [-1, -1, -1]])
        sharpen_img_1=cv2.filter2D(resized,-1,filter)
        image_blur = cv2.GaussianBlur(sharpen_img_1,(5,5),0)
        image_binary = cv2.adaptiveThreshold(image_blur,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV, 11, 12)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(5,5))
        bold_img = cv2.dilate(image_binary, kernel, iterations=2) #làm đậm edge
        tmp_img = cv2.medianBlur(bold_img, 25)

        rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1000, 3))
        dilation = cv2.dilate(tmp_img, rect_kernel, iterations = 3)
        contours, hierarchy = cv2.findContours(dilation, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        lines = []
        for cnt in contours:
            x, y, w, h = cv2.boundingRect(cnt)
            lines.append([x, y, w, h])
        lines = sorted(lines, key= lambda line : line[1])

        rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 1000))
        col_dilation = cv2.dilate(tmp_img, rect_kernel, iterations = 3)
        contours, hierarchy = cv2.findContours(col_dilation, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        cols = []
        for cnt in contours:
            x, y, w, h = cv2.boundingRect(cnt)
            cols.append([x, y, w, h])
        cols = sorted(cols, key= lambda col : col[0])

        num_chars = len(cols)*len(lines)

        contours, hierarchy = cv2.findContours(tmp_img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        contours = sorted(contours, key = lambda con : cv2.contourArea(con), reverse=True)[:num_chars]
        matrix = [[] for i in range(len(lines))]
        print(matrix)
        for con in contours:
            (x, y, w, h) = cv2.boundingRect(con)
            for i in range(len(lines)):
                if(y>=lines[i][1] and y<=lines[i][1] + lines[i][3]):
                    matrix[i].append([x,y,w,h])
                    break
        matrix = [sorted(i, key = lambda x: x[0]) for i in matrix]

        images = []
        for line in matrix:
            for char in line:
                x, y, w, h = char
                crop_img = bold_img[y:y+h, x:x+w]
                images.append(crop_img)

        finals = []
        nist_w = 28
        nist_h = 28
        for image in images:
            tmp = cv2.resize(image, (20,20))
            res = np.full((nist_w, nist_h), 0, dtype=np.float32)
            image_w, image_h = tmp.shape
            xx = (nist_w-image_w)//2
            yy = (nist_h-image_h)//2
            res[yy:yy+image_w, xx:xx+image_w] = tmp
            blur_res = cv2.GaussianBlur(res,(3, 3),0)
            blur_res /= 255.0
            finals.append(blur_res)
        return finals, len(lines), len(cols)

def plot_img(img, cmap = 'gray'): # img: numpy array
    plt.imshow(img, cmap= cmap)  #plt có cmap khác với opencv

if __name__ == '__main__':
    filepath = "D:/myproject/text_reg/test_img/so_sau.png"
    print(filepath)
    image = cv2.imread(filepath)
    res = ImageProcessor.convert_to_emnist(image)
    for i in res:
        plot_img(i)
        plt.show()
