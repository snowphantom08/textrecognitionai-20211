from PyQt5.QtCore import Qt
from PyQt5.QtGui import QImage, QPixmap, QPalette, QPainter, QFont
from PyQt5.QtWidgets import QLabel, QSizePolicy, QScrollArea, QMessageBox, QMainWindow, QMenu, QAction, \
    qApp, QFileDialog, QPushButton, QWidget, QGridLayout, QTextBrowser
from src.recognizer.predictor.character_predictor import CharacterPredictor

class QImageViewer(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setStyleSheet("background-color: #f5f5f5")
        self.font = QFont("Arial", 16, QFont.Bold)
        self.fileName = ""
        self.titleLabel = QLabel("Handwritting Recognization")
        self.titleLabel.setFixedSize(900,80)
        self.titleLabel.setFont(self.font)
        self.titleLabel.setStyleSheet("margin:200px; ")
        self.imageLabel = QLabel()
        self.imageLabel.setBackgroundRole(QPalette.Base)
        self.imageLabel.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        self.imageLabel.setScaledContents(True)
        self.imageLabel.setMaximumSize(900, 500)
        self.scrollArea = QScrollArea()
        self.scrollArea.setMaximumSize(900,500)
        self.scrollArea.setBackgroundRole(QPalette.Dark)
        self.scrollArea.setWidget(self.imageLabel)
        self.scrollArea.setVisible(False)

        self.openfile = QPushButton("Open")
        self.openfile.setFixedSize(150, 50)
        self.openfile.setStyleSheet(
            "background-color: #0000ff; color: white;margin-left: 50px;")
        self.openfile.setFont(self.font)
        self.openfile.clicked.connect(self.open)

        self.button = QPushButton("Run")
        self.button.setMaximumSize(150, 50)
        self.button.setStyleSheet(
            "background-color: #0000ff; color: white;margin-left: 50px;")
        self.button.setFont(self.font)
        self.button.clicked.connect(self.Check)

        self.text = QTextBrowser()
        self.text.setText("Result")
        self.text.setFixedWidth(800)
        self.text.setStyleSheet(
            "border-radius: 25px; background-color: #CAE5E8; font-size:25px;margin-left: 30px")
        self.text.setFont(self.font)

        self.widget = QWidget()
        self.horizontal_layout = QGridLayout()
        self.horizontal_layout.addWidget(self.titleLabel, 1, 0)
        self.horizontal_layout.addWidget(self.openfile, 2, 0)
        self.horizontal_layout.addWidget(self.scrollArea, 3, 0)
        self.horizontal_layout.addWidget(self.button, 2, 1)
        self.horizontal_layout.addWidget(self.text, 3, 1)
        self.widget.setLayout(self.horizontal_layout)
        self.setCentralWidget(self.widget)

        self.createActions()

        self.setWindowTitle("Handwritting Recognization")
        self.resize(800, 900)
        print("tải giao diện ....")
        self.model = CharacterPredictor()

    @property
    def filename(self):
        return self.fileName

    @filename.setter
    def filename(self, new_filename):
        self.fileName = new_filename

    def Check(self):
       char, p = self.model.predict_image(self.fileName)
       char = ' '.join(char)
       p = '\n'.join([str(i) for i in p])
       self.text.setText("RESULT:\n {} \n P: \n {}".format(char, p))

    def open(self):
        options = QFileDialog.Options()
        self.fileName, _ = QFileDialog.getOpenFileName(self, 'QFileDialog.getOpenFileName()', '',
                                                       'Images (*.png *.jpeg *.jpg *.bmp *.gif)', options=options)
        if self.fileName:
            image = QImage(self.fileName)
            if image.isNull():
                QMessageBox.information(
                    self, "MY APP", "Cannot load %s." % self.fileName)
                return

            self.imageLabel.setPixmap(QPixmap.fromImage(image))
            self.scaleFactor = 1.0

            self.scrollArea.setVisible(True)
            self.fitToWindowAct.setEnabled(True)

        if not self.fitToWindowAct.isChecked():
            self.imageLabel.adjustSize()

    def fitToWindow(self):
        fitToWindow = self.fitToWindowAct.isChecked()
        self.scrollArea.setWidgetResizable(fitToWindow)
        if not fitToWindow:
            self.normalSize()

    def createActions(self):
        self.fitToWindowAct = QAction("&Fit to Window", self, enabled=False, checkable=True, shortcut="Ctrl+F",
                                      triggered=self.fitToWindow)

    

if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)
    imageViewer = QImageViewer()
    imageViewer.show()
    sys.exit(app.exec_())
