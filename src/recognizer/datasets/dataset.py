from pathlib import  Path

class Dataset():
    def __init__(self):
        self.input_shape = None
        self.num_classes = None
        self.mapping = None
        self.X_train = None
        self.y_train = None
        self.X_val = None
        self.y_val = None
        self.X_test = None
        self.y_test = None
    """static method"""
    @classmethod
    def data_dir_path(self):
        return Path(__file__).resolve().parents[3] / 'data'
    def load_data(self):
        pass

