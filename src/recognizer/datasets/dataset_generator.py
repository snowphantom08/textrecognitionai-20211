from tensorflow.keras.utils import Sequence
import numpy as np
"""
Generate your dataset by batches 
and feed each right away to your deep learning model.
"""
class DatasetGenerator(Sequence):
    def __init__(self, X, y, batch_size = 128, input_shape =(28, 28), channels = 5, augment_func = None, format_func = None):
        self.X = X
        self.y = y
        self.batch_size = batch_size
        self.input_shape = input_shape
        self.channels = channels
        self.augment_func = augment_func
        self.format_func = format_func

    def __len__(self):
        """Return number of batchs"""
        # return np.ceil(len(self.X)/self.batch_size)
        return int(np.ceil(len(self.X) / float(self.batch_size)))
    def __getitem__(self, idx):
        """Return a single batch"""
        begin = idx*self.batch_size
        end = (idx+1)*self.batch_size

        batch_X = self.X[begin:end]
        batch_y = self.y[begin:end]

        """Normalize input"""
        if batch_X.dtype == np.uint8:
            batch_X = (batch_X/255).astype(np.float32)

        """Augment data"""
        if self.augment_func is not None:
            batch_X, batch_y = self.augment_func(batch_X, batch_y)

        """Format data"""
        if self.format_func is not None:
            batch_X, batch_y = self.format_func(batch_X, batch_y)

        return batch_X, batch_y



    def on_epoch_end(self):
        """Shuffle data & called at the end of every epoch"""
        self.X, self.y = _shuffle(self.X, self.y)

def _shuffle(X, y):
    index = np.random.permutation(X.shape[0])
    return X[index], y[index]