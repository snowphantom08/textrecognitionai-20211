from src.recognizer.datasets import Dataset
from tensorflow.keras.utils import to_categorical
from pathlib import Path
import pandas as pd
import json
import numpy as np
from sklearn.model_selection import train_test_split
#.resolve() : make the path become absolute
LABEL_FILENAME = Path(__file__).parents[0].resolve() / 'label_map.json'
TRAIN_DATA_PATH = Dataset.data_dir_path() / 'raw' / 'emnist' / 'emnist-balanced-train.csv'
TEST_DATA_PATH = Dataset.data_dir_path() / 'raw' / 'emnist' / 'emnist-balanced-test.csv'

class EmnistDataset(Dataset):
    def __init__(self):
        """ This file's format : {"mapping" : [i, lable]; "input_shape" : [width, height]}"""
        with open(LABEL_FILENAME) as f:
            label_map = json.load(f)
        self.mapping = dict(label_map["mapping"])
        self.num_classes = len(label_map["mapping"])
        self.input_shape = label_map["input_shape"]
        self.output_shape = (len(label_map["mapping"]),)

    def load_train_test(self):
        self.X_train, self.y_train = _img_label_load(TRAIN_DATA_PATH)
        self.X_train, self.X_val, self.y_train, self.y_val = train_test_split(self.X_train, self.y_train, test_size=0.2, random_state=42)
        self.X_test, self.y_test = _img_label_load(TEST_DATA_PATH)

def _img_label_load(data_path):
    print(data_path)
    data = pd.read_csv(data_path, header=None)
    data_rows = len(data)
    num_classes = len(data[0].unique())
    img_size = int(np.sqrt(len(data.iloc[0, 1:])))
    imgs = np.transpose(data.values[:, 1:].reshape(data_rows, img_size, img_size, 1), axes=[0, 2, 1, 3])
    label_id = data.values[:, 0]
    labels = to_categorical(label_id, num_classes)
    imgs = imgs/255.
    return imgs, labels

def main():
    dataset = EmnistDataset()
    dataset.load_train_test()

    print(dataset)
    print(dataset.X_train.shape, dataset.y_train.shape)
    print(dataset.X_val.shape, dataset.y_val.shape)
    print(dataset.X_test.shape, dataset.y_test.shape)

if __name__ == '__main__':
    main()
