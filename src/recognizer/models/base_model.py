
from typing import Callable, Dict
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import Sequential
from pathlib import Path
from src.recognizer.datasets import DatasetGenerator
from src.recognizer.datasets import Dataset
import numpy as np

WEIGHT_DIRNAME = Path(__file__).parents[1] / 'weights'
class Model:
    def __init__(self, dataset_class: Dataset, network_func = Callable, dataset_args: Dict = None, network_args: Dict = None):
        self.name = "{}_{}_{}".format(self.__class__.__name__, network_func.__name__, dataset_class.__name__)

        if dataset_args is None:
            dataset_args = {}
        self.data = dataset_class(**dataset_args)
        if network_args is None:
            network_args = {}
        self.network = network_func(self.data.input_shape, self.data.num_classes, **network_args)
        self.network.summary()

    # getter
    @property
    def input_shape(self):
        return self.data.input_shape

    @property
    def weights_filename(self):
        return WEIGHT_DIRNAME / '{}_weights.h5'.format(self.name)

    def fit_generater(self, dataset: Dataset = None, batch_size = 128, epochs = 5, is_augmented = True, callbacks = []):
        train_generator = DatasetGenerator(
            self.data.X_train if dataset is None else dataset.X_train,
            self.data.y_train if dataset is None else dataset.y_train,
            batch_size,
            input_shape= self.data.input_shape if dataset is None else dataset.input_shape,
            channels = 1,
            augment_func= None,
            format_func= None,
        )

        test_generator = DatasetGenerator(
            self.data.X_test if dataset is None else dataset.y_test,
            self.data.y_test if dataset is None else dataset.y_test,
            batch_size,
            input_shape= self.data.input_shape if dataset is None else dataset.input_shape,
            channels = 1,
            augment_func= None,
            format_func= None,
        )

        self.network.fit_generator(
            generator= train_generator,
            epochs = epochs,
            callbacks = callbacks,
            validation_data=test_generator,
            shuffle= True
        )

    def fit(self, dataset: Dataset = None, batch_size = 128, epochs = 5, is_augmented = True, callbacks = []):
        self.network.fit(
            self.data.X_train,
            self.data.y_train,
            epochs = epochs,
            batch_size = batch_size,
            verbose=1,
            validation_data=(self.data.X_val, self.data.y_val),
            callbacks = callbacks
        )

    def evaluate(self, X, y, batch_size=16):
        generator = DatasetGenerator(X, y, batch_size=batch_size)
        predict = self.network.predict_generator(generator)
        return np.mean(predict == y)

    def loss(self): # pylint: disable=no-self-use
        return 'categorical_crossentropy'

    def optimizer(self): # pylint: disable=no-self-use
        return Adam()

    def metrics(self): # pylint: disable=no-self-use
        return ['accuracy']

    def load_weights(self):
        self.network.load_weights(self.weights_filename)

    def save_weights(self):
        self.network.save_weights(self.weights_filename)
