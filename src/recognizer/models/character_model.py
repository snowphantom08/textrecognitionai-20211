from typing import Callable, Tuple
from src.recognizer.models import Model
from src.recognizer.datasets.emnist_dataset import EmnistDataset
from src.recognizer.networks.character_net_3 import character_net_3
import numpy as np
import matplotlib.pyplot as plt
class CharacterModel(Model):
    def __init__(self,
                 dataset_class = EmnistDataset,
                 network_func = character_net_3,
                 dataset_args = None,
                 network_args = None):
        super().__init__(dataset_class, network_func, dataset_args, network_args)

    def predict_image(self, image: np.ndarray) -> Tuple[str, float]:
        print("Du doan ...")
        p_arr = self.network.predict(image, batch_size=1).flatten()
        print("Du doan xong!")
        print(p_arr)
        print(image.shape)
        idx = np.argmax(p_arr)
        p_max = p_arr[idx]
        predicted_char = self.data.mapping[idx]
        return predicted_char, p_max

def main():
    model = CharacterModel()
    model.data.load_train_test()
    model.fit()
    # model.save_weights()
    # print(model.weights_filename)

if __name__ == '__main__':
    main()
