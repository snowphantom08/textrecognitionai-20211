from src.recognizer.layers.layer import Layer
import numpy as np
class DropOut(Layer):
    def __init__(self, prob =0.5):
        self.prob = prob
        self.drop_matrix = None

    def forward(self, input):
        self.drop_matrix = np.random.binomial(1, 1-self.prob, input.shape) # to match with the former state
        return input*self.drop_matrix

    def backward(self, input, grad_out):
        return grad_out * self.drop_matrix