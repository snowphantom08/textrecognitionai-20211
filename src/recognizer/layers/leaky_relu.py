from src.recognizer.layers.layer import Layer
import numpy as np
class LeakyReLU(Layer):
    def __init__(self):
        pass

    def forward(self, input):
        return np.where(input>0, input, input*0.01)

    def backward(self, input, grad_out):
        grad_lrelu = np.where(input>0, 1, 0.01)
        return grad_lrelu * grad_out
