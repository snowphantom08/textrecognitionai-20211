from src.recognizer.layers.layer import Layer
import numpy as np
import math
from src.recognizer.layers.conv_fast import im2col_indices, col2im_indices
def all_idx(idx, axis):
    grid = np.ogrid[tuple(map(slice, idx.shape))]
    grid.insert(axis, idx)
    return tuple(grid)
class MaxPooling(Layer):
    def __init__(self, window_size = 2, stride=2, padding = 1):
        self.window_size = window_size
        self.stride = stride
        self.padding = padding
        pass

    def forward(self, input):
        n, h, w, c = input.shape
        h_out = int((h - self.window_size + 2*self.padding) / self.stride +1)
        w_out = int((w - self.window_size + 2*self.padding) / self.stride +1)
        X_col = im2col_indices(input, self.window_size, self.window_size, padding=self.padding, stride=self.stride)
        self.X_col_shape = X_col.shape
        self.max_idx = np.argmax(X_col, axis=0)
        out = X_col[self.max_idx, range(self.max_idx.size)]
        out = out.reshape(h_out, w_out,n, c).transpose(2, 3, 0, 1)
        return out

    def backward(self, input, grad_out):
        grad_in_col = np.zeros(self.X_col_shape)
        grad_out_col = grad_out.transpose(2, 3, 0, 1).ravel()
        grad_in_col[self.max_idx, range(self.max_idx.size)] = grad_out_col
        grad_in = col2im_indices(grad_in_col,
                                 input.shape,
                                 self.window_size,
                                 self.window_size,
                                 self.padding, self.stride)
        return grad_in

if __name__ == '__main__':
    layer = MaxPooling()
    input = [[[[1], [2], [3], [4]],
              [[5], [6], [7], [8]],
              [[9], [10], [11], [12]]],
             [[[1], [2], [3], [4]],
              [[5], [6], [7], [8]],
              [[9], [10], [11], [12]]]]
    # input = np.ones((32, 28, 28, 3))
    input = np.array(input).reshape((2, 3, 4, 1))
    out = layer.forward(input)
    print(out.shape)
    next = layer.backward(input, out)


