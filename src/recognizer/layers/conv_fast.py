from src.recognizer.layers.layer import Layer
import numpy as np
import math
np.random.seed(8)

def get_im2col_indices(x_shape, field_height, field_width, padding=1, stride=1):
    # First figure out what the size of the output should be
    N, H, W, C = x_shape
    # assert (H + 2 * padding - field_height) % stride == 0
    # assert (W + 2 * padding - field_height) % stride == 0
    out_height = int((H + 2 * padding - field_height) / stride + 1)
    out_width = int((W + 2 * padding - field_width) / stride + 1)

    i0 = np.repeat(np.arange(field_height), field_width)
    i0 = np.tile(i0, C)
    i1 = stride * np.repeat(np.arange(out_height), out_width)
    j0 = np.tile(np.arange(field_width), field_height * C)
    j1 = stride * np.tile(np.arange(out_width), out_height)
    i = i0.reshape(-1, 1) + i1.reshape(1, -1)
    j = j0.reshape(-1, 1) + j1.reshape(1, -1)

    k = np.repeat(np.arange(C), field_height * field_width).reshape(-1, 1)

    return (k, i, j)


def im2col_indices(x, field_height, field_width, padding=1, stride=1):
    """ An implementation of im2col based on some fancy indexing """
    # Zero-pad the input
    p = padding
    x_padded = np.pad(x, ((0, 0), (p, p), (p, p), (0, 0)), mode='constant')
    k, i, j = get_im2col_indices(x.shape, field_height, field_width, padding,
                                 stride)

    cols = x_padded[:, i, j, k]
    C = x.shape[3]
    cols = cols.transpose(1, 2, 0).reshape(field_height * field_width, -1)
    return cols


def col2im_indices(cols, x_shape, field_height=3, field_width=3, padding=1,
                   stride=1):
    """ An implementation of col2im based on fancy indexing and np.add.at """
    N, H, W, C = x_shape
    H_padded, W_padded = H + 2 * padding, W + 2 * padding
    x_padded = np.zeros((N, H_padded, W_padded, C), dtype=cols.dtype)
    k, i, j = get_im2col_indices(x_shape, field_height, field_width, padding,
                                 stride)
    cols_reshaped = cols.reshape(C*field_height * field_width, -1, N)
    cols_reshaped = cols_reshaped.transpose(2, 0, 1)
    np.add.at(x_padded, (slice(None),i, j, k), cols_reshaped)
    if padding == 0:
        return x_padded
    return x_padded[:, padding:-padding, padding:-padding, :]
class Conv2dFast(Layer):
    def __init__(self, num_filers = 3, kernel_size = 2, stride = 1, input_shape = (28,28), padding = 1):
        self.num_filters = num_filers
        self.kernel_size = kernel_size
        self.stride = stride
        self.filters = np.random.randn(num_filers, kernel_size * kernel_size)/(kernel_size * kernel_size)
        self.learning_rate = 0.01
        self.input_shape = input_shape
        self.padding = padding

    def image_region(self, input):
        input = input.reshape(input.shape[0], self.input_shape[0], self.input_shape[1])
        w, h = self.input_shape
        for i in range(h - self.kernel_size +1):
            for j in range(w - self.kernel_size +1):
                frag = input[:, i:(i+self.kernel_size), j:(j+self.kernel_size)]
                yield frag.reshape(frag.shape[0], self.kernel_size**2), i, j

    def forward(self, input):
        n, w, h, c = input.shape
        h_out = int((h-self.kernel_size +2*self.padding) / self.stride +1)
        w_out = int((w-self.kernel_size +2*self.padding) / self.stride +1)
        self.X_col = im2col_indices(input, self.kernel_size, self.kernel_size, padding=self.padding, stride=self.stride)
        W_col = self.filters.reshape(self.num_filters, self.kernel_size*self.kernel_size)
        output = W_col @ self.X_col

        out = output.reshape(self.num_filters*c, n, w_out, h_out)
        out = out.transpose(1, 2, 3, 0)
        return out

    def backward(self, input, grad_out):
        n, _, _, _ = grad_out.shape
        grad_out = grad_out.transpose(1, 2, 3, 0).reshape(self.num_filters, -1)
        W_col = self.filters.reshape(self.num_filters, -1)

        grad_w = grad_out @ self.X_col.T # 3, 4
        grad_in_col = (W_col.T @ grad_out).reshape(self.X_col.shape)  # 4, 2523
        grad_in = col2im_indices(grad_in_col, input.shape, self.kernel_size, self.kernel_size, padding=1, stride=1)
        W_col -= self.learning_rate*grad_w
        self.filters = W_col.reshape(self.num_filters, self.kernel_size, self.kernel_size)
        return grad_in
if __name__ == "__main__" :
    input = [[[[1], [2], [3], [4]],
              [[5], [6], [7], [8]],
              [[9], [10], [11], [12]]],
             [[[1], [2], [3], [4]],
              [[5], [6], [7], [8]],
              [[9], [10], [11], [12]]]]

    input = np.array(input).reshape((2, 3, 4, 1))
    # print(input)
    layer = Conv2dFast()
    output = layer.forward(input)

    # print(output)
    grad_in = layer.backward(input, output)
    print(grad_in.shape)

    # input = np.array(input).reshape((2, 3, 4, 1))
    # print(input)
    # print(im2col_indices(input, 3, 3))
    # input = np.ones((4, 21600))
    # print()