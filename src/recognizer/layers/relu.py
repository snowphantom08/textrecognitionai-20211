from src.recognizer.layers.layer import Layer
import numpy as np

class ReLU(Layer):
    def __init__(self):
        pass

    def forward(self, input): # input: batch x num_inunits
        return np.maximum(0, input) # differ with np.max

    def backward(self, input, grad_out):
        grad_relu = input > 0
        return grad_relu*grad_out

