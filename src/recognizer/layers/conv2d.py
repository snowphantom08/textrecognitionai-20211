
from src.recognizer.layers.layer import Layer
import numpy as np
import math
np.random.seed(8)
class Conv(Layer):
    def __init__(self, num_filers = 3, kernel_size = 2, stride = 1, input_shape = (3,3)):
        self.num_filters = num_filers
        self.kernel_size = kernel_size
        self.stride = stride
        self.filters = np.random.normal(loc=0.0,
                                        scale=np.sqrt(2/(kernel_size * kernel_size)),
                                        size = (num_filers, kernel_size, kernel_size))
        # print(self.filters)
        self.learning_rate = 0.01
        self.input_shape = input_shape

    def image_region(self, input):
        n, w, h, c = input.shape
        for i in range(h - self.kernel_size +1):
            for j in range(w - self.kernel_size +1):
                frag = input[:, i:(i+self.kernel_size), j:(j+self.kernel_size), :]
                yield frag, i, j

    def forward(self, input):
        n, w, h, c = input.shape
        output = np.zeros((n, self.num_filters, (h-self.kernel_size+1), (w-self.kernel_size+1)))
        for frag, i, j in self.image_region(input):
            f_flat = self.filters.reshape(self.num_filters, -1)
            for k in range(n):
                tmp = np.dot(f_flat, frag[k].reshape(-1, c)).ravel()
                output[k, :, i, j] = tmp
        return output.reshape(n, self.num_filters, (h-self.kernel_size+1), (w-self.kernel_size+1)).transpose(0, 3, 1, 2)

    def backward(self, input, grad_out):
        n, w, h, c = grad_out.shape
        grad_out = grad_out.transpose(0, 2, 3, 1)
        grad_in = np.zeros(input.shape)
        for frag, i, j in self.image_region(input):
            for k in range(self.num_filters):
                frag.reshape(frag.shape[0], self.kernel_size**2)
                grad_in[:, :, i: i+self.kernel_size, j : j+ self.kernel_size] += frag * grad_out[:, k, i, j]

        self.filters -= self.learning_rate*np.mean(grad_in, axis=0)
        return grad_in.reshape(n, self.num_filters * grad_in.shape[-1])

if __name__ == "__main__" :
    input = np.ones((10, 28, 28, 1))
    # print(input)
    layer = Conv()
    output = layer.forward(input)
    # print(output)
    grad_in = layer.backward(input, output)
    print(grad_in.shape)


    # x = np.ones((1, 9))
    # y = np.ones((12, 9))*9
    # z = x*y
    # print(z.shape)



