from src.recognizer.layers.layer import Layer
import numpy as np

class Flatten(Layer):
    def __init__(self):
        pass

    def forward(self, input):
        self.input_shape = input.shape
        return input.reshape(input.shape[0], np.prod(list(input.shape)[1:]))

    def backward(self, input, grad_out):
        return grad_out.reshape(self.input_shape)


if __name__ == '__main__':
    input = [[[[1], [2], [3], [4]],
              [[5], [6], [7], [8]],
              [[9], [10], [11], [12]]],
             [[[1], [2], [3], [4]],
              [[5], [6], [7], [8]],
              [[9], [10], [11], [12]]]]
    input = np.array(input).reshape((2, 3, 4, 1))
    layer = Flatten()
    input = layer.backward(input, layer.forward(input))
    print(input)
