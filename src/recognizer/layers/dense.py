from src.recognizer.layers.layer import Layer
import numpy as np
class Dense(Layer):
    def __init__(self, num_inunits, num_outunits, learning_rate=0.01):
        self.learning_rate=0.01
        """
        loc: kỳ vọng của phân phối
        scale: phương sai
        khởi tạo w tuân theo phân phối chuẩn ~ Batch Normalization
        size of weight: input x ouput
        """
        self.weights = np.random.normal(loc=0.0,
                                        scale=np.sqrt(2/(num_inunits+num_outunits)),
                                        size = (num_inunits, num_outunits))
        # print("weight")
        # print(self.weights)
        self.biases = np.zeros(num_outunits) # shape: 1 x num_outunits
    """
    design for mini-batch
    """
    def forward(self, input): #input_shape: batch x num_inunits
        """
        compute: f(x) = W*x +b
        """
        return np.dot(input, self.weights) + self.biases

    def backward(self, input, grad_out): # input <-- output
        grad_in = np.dot(grad_out, self.weights.T) # shape grad_out: batch x num_outunits
        grad_weights = np.dot(input.T, grad_out)   # num_inunits x num_outunits ### tage the average??????
        """
        bias for all input items in batch will be the same 
        """
        grad_biases = grad_out.mean(axis=0)*input.shape[0] # batch x num_outunits
        self.weights = self.weights - self.learning_rate*grad_weights
        self.biases = self.biases - self.learning_rate*grad_biases
        return grad_in









