from src.recognizer.layers.layer import Layer
import numpy as np
class BatchNorm(Layer):
    def __init__(self, learning_rate = 0.01, epsilon = 0.001):
        """
        gamma : scaling factor
        beta : shift
        """
        self.learning_rate= learning_rate
        self.gamma = 1 # like keras
        self.beta = 0
        self.epsilon = epsilon

    def forward(self, input):
        mean = np.mean(input, axis=0)
        xm = input-mean
        sub = xm**2
        var = np.mean(sub, axis=0)
        sqrtvar = np.sqrt(var+self.epsilon)
        var_inv = 1.0/sqrtvar
        x_new = sub * var_inv
        y = self.gamma * x_new + self.beta
        cache = (x_new, self.gamma, sub, xm, var_inv, sqrtvar, var, self.epsilon)
        output = [y, cache]
        return output

    def backward(self, input, grad_out): # input : la output cua layer trong buoc forward
        x_new, gamma, sub, xm, var_inv, sqrtvar, var, eps = input[1]
        batch_size = input.shape[0]
        num_inunits = input.shape[1]
        #out = gammax + beta
        #dl_gammax = dl_out * dout_gammax
        grad_gammax = grad_out * 1
        # grad_beta = np.sum(grad_out, axis=0)

        grad_xnew = grad_gammax * gamma
        # grad_gamma = np.dot(x_new, grad_gammax)


        grad_xm1 = grad_xnew * (-1) * var_inv
        grad_var_inv = np.dot(sub, grad_xnew)

        grad_sqrtvar = grad_var_inv * (-1.0/(sqrtvar**2))

        grad_var = grad_sqrtvar * (0.5/np.sqrt(var + eps))
        grad_sub = 1.0/batch_size * np.ones((batch_size, num_inunits))*grad_var

        grad_xm2 = grad_sub * 2 * xm

        grad_x1 = grad_xm2 + grad_xm1
        grad_mean = -1 * np.sum(grad_xm2+grad_xm1, axis = 0)

        grad_x2 = 1.0/batch_size * np.ones((batch_size, num_inunits)) * grad_mean

        grad_x = grad_x1 + grad_x2
        # self.gamma = self.gamma - self.learning_rate* grad_gamma
        # self.beta = self.beta - self.learning_rate*grad_beta
        return grad_x





