from typing import Callable, Tuple
import pickle
from tensorflow.python.keras.utils.np_utils import to_categorical

from src.recognizer.layers.conv_fast import Conv2dFast
from src.recognizer.layers.dropout import DropOut
from src.recognizer.layers.flatten import Flatten
from src.recognizer.layers.leaky_relu import LeakyReLU
from src.recognizer.layers.maxpooling import MaxPooling
from src.recognizer.models import Model
from src.recognizer.datasets.emnist_dataset import EmnistDataset
from src.recognizer.networks.character_net_2 import character_net_2
import numpy as np
import matplotlib.pyplot as plt
from src.recognizer.layers.dense import Dense
from src.recognizer.layers.relu import ReLU
from src.recognizer.layers.batch_norm import BatchNorm

class MLPNetwork:
    def __init__(self, input_shape: Tuple[int, ...] , num_classes, learning_rate=0.1):
        self.input_shape = input_shape
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        net = []
        net.append(Conv2dFast(num_filers=3, kernel_size=3, stride=1, input_shape=(28, 28)))
        net.append(ReLU())
        net.append(MaxPooling(window_size=2, stride=2, padding=1))
        net.append(Flatten())
        net.append(Dense(675, 100))
        net.append(ReLU())
        net.append(DropOut(0.1))
        net.append(Dense(100, 47))
        # net.append(LeakyReLU())
        # net.append(DropOut(0.1))
        # net.append(Dense(100, 47))
        self.network = net
        self.history = {'val_accuracy': [], 'train_accuracy': []}

    def forward(self, X):
        layer_inputs = []
        layer_inputs.append(X)
        input = X
        idx = 0
        for layer in self.network:
            input= layer.forward(input)
            layer_inputs.append(input)
            idx+=1
        return layer_inputs

    def fit(self, X_train, y_train, epochs = 20,
            batch_size = 1,
            verbose=1,
            X_val = None,
            y_val = None,
            callbacks = None):
        # X_train = X_train.reshape([X_train.shape[0], -1])
        y_train = y_train.reshape([y_train.shape[0], -1])
        # X_val = X_val.reshape([X_val.shape[0], -1])
        y_val = y_val.reshape([y_val.shape[0], -1])

        for epoch in range(epochs):
            losses = []
            for X_batch, y_batch in iterate_minibatch(X_train, y_train, batch_size, shuffle=True):
                layer_inputs = self.forward(X_batch)
                logits = layer_inputs[-1]
                loss = softmax_crossentropy(logits, y_batch)
                losses.append(loss)
                grad_loss = grad_softmax_crossentropy(logits, y_batch)
                for idx in range(len(self.network))[::-1]:
                    layer = self.network[idx]
                    grad_loss = layer.backward(layer_inputs[idx], grad_loss)
            print("Predict")
            print(self.predict(X_val))
            print("Y_val")
            print(np.argmax(y_val, axis=-1))
            print("Loss")
            print(np.mean(np.array(losses)))
            val_accuracy = np.mean(self.predict(X_val)==np.argmax(y_val, axis=-1))
            print("Val_accuracy {}".format(val_accuracy))
            self.history['val_accuracy'].append(val_accuracy)
            accuracy = np.mean(self.predict(X_train)==np.argmax(y_train, axis=-1))
            print("Accuracy {}".format(accuracy))
            self.history['train_accuracy'].append(np.mean(self.predict(X_train)==np.argmax(y_train, axis=-1)))
        print(self.history )

    def predict(self, X):
        logits = self.forward(X)[-1]
        # print("Logits")
        # print(logits)
        return np.argmax(logits, axis = -1)

"""
    Trả về generator (X,y) cho từng batch
"""
def iterate_minibatch(X, y, batch_size, shuffle=False):
    if(batch_size > len(X)):
        yield X, y
        return
    if shuffle:
        indices = np.random.permutation(X.shape[0])

    for start_idx in range(0, len(X)-batch_size+1, batch_size):
        if shuffle:
            idx_batch = indices[start_idx: start_idx+batch_size]

        else:
            idx_batch = slice(start_idx, start_idx+batch_size)

        yield X[idx_batch], y[idx_batch]


def softmax_crossentropy(logits, classes_true):
    """
    shape logits : batch x num_classes
    cross_entropy = -log( exp(sp)/ (exp(s0) + exp(s1) + ...) )
    """
    sp = []
    for i, logit in enumerate(logits):
        sp.append(logit[classes_true[i]==1])
    sp = np.array(sp)
    tmp = np.exp(logits)
    loss = - sp + np.log(np.sum(tmp, axis=-1))
    return loss

def grad_softmax_crossentropy(logits, classes_true):
    grad_sp = np.zeros_like(logits)
    grad_sp[classes_true==1] = 1
    # logits = logits - np.max(logits)
    tmp = np.exp(logits)
    grad_sum =  tmp / tmp.sum(axis=-1, keepdims = True)
    return (- grad_sp + grad_sum)

if __name__ == '__main__':
    dataset = EmnistDataset()
    dataset.load_train_test()
    X_train, y_train, X_val, y_val = dataset.X_train, dataset.y_train, dataset.X_val, dataset.y_val
    # print(np.argmax(y_val, axis=-1))
    # input = [[[[1], [2], [3], [4]],
    #           [[5], [6], [7], [8]],
    #           [[9], [10], [11], [12]]],
    #          [[[1], [2], [3], [4]],
    #           [[5], [6], [7], [8]],
    #           [[9], [10], [11], [12]]]]
    # input = np.array(input).reshape((2, 3, 4, 1))
    # y = np.array([2, 3])
    model = MLPNetwork([1, 784], 47)
    model.fit(X_train, y_train, X_val= X_val, y_val= y_val)
    # model.fit(input, y)


