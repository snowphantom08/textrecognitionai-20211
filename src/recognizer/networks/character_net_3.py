from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Dense, AveragePooling2D, BatchNormalization
from typing import Tuple
def character_net_3(input_shape: Tuple[int, ...] , num_classes):
    model = Sequential()

    model.add(Conv2D(32,kernel_size=3,padding='same',activation= 'relu',
                     input_shape=(28,28,1)))
    model.add(MaxPooling2D(pool_size = (2,2)))
    model.add(BatchNormalization())
    model.add(Conv2D(64,kernel_size=3,padding='same',activation= 'relu'))
    model.add(MaxPooling2D(pool_size = (2,2)))
    model.add(BatchNormalization())
    model.add(Conv2D(128,kernel_size=3,padding='same',activation= 'relu'))
    model.add(MaxPooling2D(pool_size = (2,2)))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dense(512, activation= 'relu'))
    model.add(Dropout(0.5))
    model.add(Dense(128, activation= 'relu'))
    model.add(Dense(units = num_classes, activation = 'softmax'))
    model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["accuracy"])
    return model