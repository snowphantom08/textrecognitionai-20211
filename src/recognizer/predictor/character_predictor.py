
import cv2
from src.recognizer.models.character_model import CharacterModel
from src.preprocessor.image_processor import ImageProcessor
import matplotlib.pyplot as plt
class CharacterPredictor():
    def __init__(self):
        self.model = CharacterModel()
        self.model.load_weights()
        pass

    def predict_image(self, filepath = ""):
        images, rows, cols = ImageProcessor.convert_to_emnist(filepath)
        chars_pred = []
        ps_pred = []
        print("Du doan anh...")
        for image in images:
            image = image.reshape([-1] + self.model.input_shape)
            char, p = self.model.predict_image(image)
            chars_pred.append(char)
            ps_pred.append(p)
        return chars_pred, ps_pred



if __name__ == '__main__':
    predictor = CharacterPredictor()
    image_filepaths = ["D:/myproject/text_reg/test_img/ma_tran_chu.jpg"]
    for i in image_filepaths:
        print(i)
        chars, ps = predictor.predict_image(i)
        print(chars)
        print(ps)