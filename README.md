# Handwritting Recognization Application

Ứng dụng nhận diện kí tự viết tay bằng kí tự Latinh sử dụng mô hình mạng CNN với bộ dữ liệu EMNIST. 

## CNN Model
**EMNIST Dataset**
Bộ dữ liệu bao gồm 131600 ảnh, phân bố đều trên 47 nhãn.
Bộ dữ liệu được chia như sau:

 - Tập train:  (90240, 28, 28, 1)  Để huấn luyện

- Tập validation :  (22560, 28, 28, 1)  Để chọn mô hình

- Tập test : (18800, 28, 28, 1)  Để đánh giá mô hình đã chọn

Nhãn đầu ra: Gồm 47 nhãn bao gồm các ký tự sau:

**0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabdefghnqrt**

**Training**
- Cấu trúc mô hình
![alt](img/model.png)
- Confusion matrix
 [!alt]
- So sánh với một số mô hình khác
[!alt]

## Dependencies
 - Python 3.8.6 or later
 - keras, cv2, tensorflow, numpy, PyQt5 

## Run App
chạy hàm main nằm ở trong file sau:
main: [src/recognizer/view/gui.py](src/recognizer/view/gui.py)
 
Để chọn ảnh từ thiết bị nhấn **Open**
![alt](img/open.png)

Nhấn **Check** ứng dụng sẽ đưa ra kết quả ở ô **RESULT** như hình:
![alt](img/result.png)
## Reference
Tham khảo tổ chức cấu trúc ứng dụng:

https://github.com/Zhenye-Na/crnn-pytorch 